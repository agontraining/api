FROM elixir:latest

RUN mkdir /app
WORKDIR /app

COPY . .

RUN mix deps.get

CMD iex -S mix