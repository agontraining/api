defmodule Agon.Router do
  use Plug.Router
  plug :match
  plug :dispatch

  get "/" do
    {:ok, bin, conn} = read_body(conn,length: 100_000_000)

    conn |> put_resp_header("Access-Control-Allow-Origin", "*")
    |> send_resp(200, "OK\n")
    |> halt()
  end

  get "/*path" do
    {:ok, bin, conn} = read_body(conn,length: 100_000_000)

    conn |> put_resp_header("Access-Control-Allow-Origin", "*")
    |> send_resp(404, "Not found\n")
    |> halt()
  end
end
